version: "3.7"

services:

  rabbit-mq:
    image: library/rabbitmq:3-management
    container_name: rabbitMQ
    hostname: ${RABBITMQ_HOSTNAME}
    networks:
      - backend
    ports:
      - 5672:5672
      - 15672:15672
    environment:
      RABBITMQ_DEFAULT_USER: ${RABBIT_MQ_USER}
      RABBITMQ_DEFAULT_PASS: ${RABBIT_MQ_PASS}

  mongo-db:
    image: mongo
    container_name: mongoDB
    hostname: ${MONGODB_HOSTNAME}
    networks:
      - backend
    ports:
      - ${MONGO_BD_PORT}:27017
    volumes:
      - fleet_mongo_db_data:/data/db

  zipkin:
    image: openzipkin/zipkin
    container_name: zipkin
    hostname: ${ZIPKIN_HOSTNAME}
    networks:
      - backend
    ports:
      - ${ZIPKIN_PORT}:9411

  config-server:
    image: ${CONFIG_SERVER_IMAGE}
    container_name: api-config-server
    hostname: ${CONFIG_HOSTNAME}
    networks:
      - backend
    ports:
      - ${CONFIG_SERVER_PORT}:${CONFIG_SERVER_EXPOSED_PORT}
    environment:
      SPRING_CLOUD_CONFIG_SERVER_GIT_USERNAME: ${SPRING_CLOUD_CONFIG_SERVER_GIT_USERNAME}
      SPRING_CLOUD_CONFIG_SERVER_GIT_PASSWORD: ${SPRING_CLOUD_CONFIG_SERVER_GIT_PASSWORD}
      SPRING_CLOUD_CONFIG_SERVER_GIT_URI: ${SPRING_CLOUD_CONFIG_SERVER_GIT_URI}
      SPRING_PROFILES_ACTIVE: ${PROFILE}
      SERVER_PORT: ${CONFIG_SERVER_PORT}

  api-gateway:
    image: ${API_GATEWAY_IMAGE}
    container_name: api-gateway
    hostname: ${GATEWAY_HOSTNAME}
    networks:
      - backend
    ports:
      - ${API_GATEWAY_PORT}:${DEFAULT_EXPOSED_PORT}
    environment:
      SPRING_PROFILES_ACTIVE: ${PROFILE}
      SERVER_PORT: ${DEFAULT_EXPOSED_PORT}
      SPRING_CLOUD_CONFIG_URI: ${CONFIG_SERVER_URI}
      MONGODB_URI: ${MONGODB_URI}
      RABBITMQ_HOST: ${RABBITMQ_HOSTNAME}
      RABBITMQ_USERNAME: ${RABBIT_MQ_USER}
      RABBITMQ_PASSWORD: ${RABBIT_MQ_PASS}
      ZIPKIN_BASE_URL: ${ZIPKIN_URI}
      DOCKER_IMAGE_EXPOSED_PORT: ${DEFAULT_EXPOSED_PORT}
      PROJECT_JWT_SECRET: ${JWT_SECRET}
      PROJECT_JWT_EXPIRATION: ${JWT_EXPIRATION}
      PROJECT_SECURITY_USER_RETRIEVE_URI: ${USER_RETRIEVE_URI}

      GATEWAY_SERVER_HOST: ${GATEWAY_HOSTNAME}

      WAIT_HOSTS: ${WAIT_HOSTS}
    depends_on:
      - config-server
    links:
      - config-server

  fleet-api-authorization-server:
    image: ${API_AUTHORIZATION_SERVER_IMAGE}
    container_name: ${AUTHORIZATION_SERVER_HOSTNAME}
    hostname: ${AUTHORIZATION_SERVER_HOSTNAME}
    networks:
      - backend
    ports:
      - ${API_AUTHORIZATION_SERVER_PORT}:${DEFAULT_EXPOSED_PORT}
    environment:
      SPRING_PROFILES_ACTIVE: ${PROFILE}
      SERVER_PORT: ${DEFAULT_EXPOSED_PORT}
      SPRING_CLOUD_CONFIG_URI: ${CONFIG_SERVER_URI}
      MONGODB_URI: ${MONGODB_URI}
      RABBITMQ_HOST: ${RABBITMQ_HOSTNAME}
      RABBITMQ_USERNAME: ${RABBIT_MQ_USER}
      RABBITMQ_PASSWORD: ${RABBIT_MQ_PASS}
      ZIPKIN_BASE_URL: ${ZIPKIN_URI}
      DOCKER_IMAGE_EXPOSED_PORT: ${DEFAULT_EXPOSED_PORT}
      PROJECT_JWT_SECRET: ${JWT_SECRET}
      PROJECT_JWT_EXPIRATION: ${JWT_EXPIRATION}
      WAIT_HOSTS: ${WAIT_HOSTS}
    depends_on:
      - config-server
    links:
      - config-server

volumes:
  fleet_mongo_db_data:

networks:
  backend:
    driver: bridge


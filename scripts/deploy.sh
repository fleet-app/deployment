#!/usr/bin/env bash

stacks=("traefik" "portainer" "jaeger" "rabbitmq" "mongodb" "config-server")

for stack in "${stacks[@]}"; do
    docker stack deploy -c ./../"${stack}"/docker-stack.yml "${stack}"
done


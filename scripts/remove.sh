#!/usr/bin/env bash

stacks=("jaeger" "rabbitmq" "mongodb" "config-server" "portainer"  "traefik")

for stack in "${stacks[@]}"; do
    docker stack rm "${stack}"
done

